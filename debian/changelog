python-etesync (0.12.1-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Remove extraneous dependency on python3-appdirs (Closes: #1067991).

 -- Alexandre Detiste <tchet@debian.org>  Wed, 20 Nov 2024 16:31:38 +0100

python-etesync (0.12.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Remove extraneous dependency on python3-six.

 -- Alexandre Detiste <tchet@debian.org>  Tue, 02 Apr 2024 08:55:13 +0200

python-etesync (0.12.1-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * stop explicitly avoid integration tests during build (done upstream now)

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 25 Aug 2020 18:09:59 +0200

python-etesync (0.11.1-3) unstable; urgency=medium

  * simplify source script copyright-chceck
  * copyright: update coverage
  * declare compliance with Debian Policy 4.5.0
  * declare buildsystem explicitly (not in environment)
  * use debhelper compatibility level 13 (not 12)

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 25 Aug 2020 18:03:41 +0200

python-etesync (0.11.1-2) unstable; urgency=medium

  * avoid checking integration tests
  * stop tolerate test failures when targeting experimental
  * copyright: drop obsolete comment about vague licensing

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Apr 2020 15:54:53 +0200

python-etesync (0.11.1-1) experimental; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * stop build-depend on python3-scrypt
  * watch: use dversionmangle=auto
  * copyright: update coverage
  * fix build-depend on python3-cryptography python3-furl python3-peewee
    python3-pytest python3-requests python3-vobject
  * relax to depend unversioned on
    python3-appdirs python3-asn1crypto python3-cffi python3-furl
    python3-orderedmultidict python3-packaging python3-peewee
    python3-py python3-pyasn1 python3-pycparser
    python3-requests python3-six python3-vobject:
    needed version satisfied even in oldstable

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Apr 2020 14:02:29 +0200

python-etesync (0.9.2-1) experimental; urgency=low

  * initial packaging release
    closes: bug#951234

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 13 Feb 2020 15:02:29 +0100
